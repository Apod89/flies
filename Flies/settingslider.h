#ifndef SETTINGSLIDER_H
#define SETTINGSLIDER_H

#include <QWidget>
#include <QString>
class QLabel;
class QSlider;
class QGroupBox;
class SettingSlider : public QWidget
{
    Q_OBJECT 
public:
    explicit SettingSlider(QString title, int min, int max, QWidget *parent = nullptr);
    int value() const;
    void setTickInterval(int ti);
    void setMaximum(int ti);

private:
    QLabel* label;
    QSlider* slider;
    QGroupBox* groupBox;

};

#endif // SETTINGSLIDER_H
