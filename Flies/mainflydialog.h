#ifndef MAINFLYDIALOG_H
#define MAINFLYDIALOG_H

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QVariantAnimation>
#include <QPixmap>
#include <QVector>
#include <memory>
#include <functional>
#include <vector>
#include "flygraghicsitem.h"
#include "fly.h"
class Field;
class SettingSlider;

using DeleterT = decltype(std::mem_fun(&QObject::deleteLater));

class MainFlyWidget : public QWidget
{
    Q_OBJECT

    QGraphicsScene mainScene;
    QGraphicsView*  view;
    SettingSlider* fieldSizeInCells;
    SettingSlider* cellFlyVolume;
    SettingSlider* flyIntelegence;
    QPushButton* applyButton;
    QPushButton* stopButton;

    std::unique_ptr<Field> field;
    std::vector<std::unique_ptr<Fly, DeleterT> > fliesOnField;
    std::vector<std::unique_ptr<QGraphicsItem> > staticGraphicItems;

    void addNewFly(QPoint cell);

    void drawGrid();
    void initUi();
    void clearField();

    void mousePressEvent(QMouseEvent* event) override;
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void onApplyButtonCLick();
    void onStopButtonCLick();

public:
    MainFlyWidget(QWidget *parent = nullptr);
};



#endif // MAINFLYDIALOG_H
