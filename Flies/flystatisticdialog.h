#ifndef FLYSTATISTICDIALOG_H
#define FLYSTATISTICDIALOG_H

#include <QDialog>
#include <QTableWidget>
#include <QPushButton>

class FlyStatisticDialog : public QDialog
{
    Q_OBJECT
public:
    FlyStatisticDialog(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    void addItem(int no, qreal vel, qreal dist, bool die);
private:
    QTableWidget *table;
    QPushButton *okButton;

    void addItemInternal(const QString& cellText, int col);
};

#endif // FLYSTATISTICDIALOG_H
