#ifndef FIELD_H
#define FIELD_H
#include <QtGlobal>
#include <QPointF>
#include <QSize>
#include <QVector>

class QGraphicsView;

class Field
{
    QSize size_;
    int cellInRowCount_;
    int cellCapacity_;
    QSize cellSize_;
    const QGraphicsView* view_;
    QVector<int> cellsCap_;


    int convertPosToNo(QPoint cell) const;
    QPoint convertNoToPos(int cellNo) const;
    bool checkCell(QPoint cell) const;

public:
    Field(QSize size, int cellInRow, int cellCapacity, QGraphicsView* view);

    QPointF cellPosOnScene(QPoint cell) const;
    QPoint getCellWithPoint(QPoint point) const;
    QPoint getRandomFreeCell() const;


    bool freeAndBookCell(QPoint srcCell, QPoint dstCell);
    bool bookCell(QPoint cell);

    int cellsCountInRaw() const;
    int width() const;
    int height() const;
    int cellWidth() const;
    int cellHeight() const;
    void setFieldSize(QSize size);
};

#endif // FIELD_H
