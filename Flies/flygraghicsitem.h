#ifndef FLYVIEW_H
#define FLYVIEW_H

#include <QVector2D>
#include "animatedpixmap.h"

class QPropertyAnimation;
class QParallelAnimationGroup;
class QGraphicsItem;

struct PicPath{
    QString moveFrame1;
    QString moveFrame2;
    QString diedFrame;
};

class FlyGraghicsItem : public AnimatedPixmap
{
    Q_OBJECT
private:
    PicPath framesPath;
    QPropertyAnimation* moveAnim;
    QPropertyAnimation* wingAnim;
    QParallelAnimationGroup* comboAnim;
    QVector2D picOrient;


    void initAnimation();
    void rotateToCoord(qreal x, qreal y);

public:
    explicit FlyGraghicsItem(PicPath picsPath, QGraphicsItem* parent = nullptr);
    void moveTo(qreal x, qreal y);
    void setDied();

    static int maxPicFlySize();
};

#endif // FLYVIEW_H
