#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QRandomGenerator>
#include "flygraghicsitem.h"
#include "fly.h"
#include <QDebug>

Fly::Fly(QPoint pos, Field *field, quint32 brainMs, QObject* parent) : QObject(parent),
    posInField_(pos),
    field_(field),
    brainMs_(brainMs),
    fv_(nullptr),
    liveTime_(0),
    distance_(0.0),
    velocity_(0.0),
    died(false),
    isFlyStopped(false)
{
    Q_ASSERT(field!=nullptr);
    connect(&timer_, &QTimer::timeout, this, &Fly::updatePosition);
    timer_.start(brainMs_ + 600); // Animation duration is 600 ms
 }

void Fly::setView(FlyGraghicsItem *fv)
{
    Q_ASSERT(fv != nullptr);
    auto pos = field_->cellPosOnScene(posInField_);
    fv_ = fv;
    fv_->setPos(pos.x() - fv_->boundingRect().width()/2, pos.y()-fv_->boundingRect().height()/2);
}

qreal Fly::velocity() const
{
    return  velocity_;
}

qreal Fly::distance() const
{
    return distance_;
}

bool Fly::isDied() const
{
    return died;
}

void Fly::stopFly()
{
    isFlyStopped = true;
}

QPoint Fly::chooseNewCell()
{
    QPoint newCell = posInField_;
    while(newCell == posInField_)
    {
        newCell.setX(getRandVal(posInField_.x()));
        newCell.setY(getRandVal(posInField_.y()));
    }
    return newCell;
}

void Fly::updatePosition()
{
    liveTime_ += brainMs_;
    if(liveTime_ < brainMs_ * field_->cellsCountInRaw() && !isFlyStopped)
    {
        auto newCell = chooseNewCell();
        if (field_->freeAndBookCell(posInField_,newCell))
        {
            auto curPos = fv_->pos();
            auto xyPos = field_->cellPosOnScene(newCell);
            auto halfOffset =  fv_->boundingRect().size()/2;
            fv_->moveTo(xyPos.x() - halfOffset.width(), xyPos.y() - halfOffset.height());
            posInField_ = newCell;
            QVector2D vec(QPointF(curPos - xyPos));
            distance_ += vec.length();
         }
         velocity_ =distance_ / liveTime_;

    }
    else if(isFlyStopped)
    {
        timer_.stop();
    }
    else
    {
        timer_.stop();
        died = true;
        fv_->setDied();
    }

}

qint32 Fly::getRandVal(qint32 in)
{
    if(in == 0)
        return QRandomGenerator::global()->bounded(2);
    else if (in == (field_->cellsCountInRaw() - 1))
        return (field_->cellsCountInRaw() - 1 - QRandomGenerator::global()->bounded(2));
    else
        return (in - 1 + QRandomGenerator::global()->bounded(3));
}
