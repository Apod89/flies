#ifndef FLY_H
#define FLY_H
#include <QObject>
#include <QTimer>
#include "field.h"
class FlyGraghicsItem;

class Fly : public QObject
{
    Q_OBJECT
public:
    explicit Fly(QPoint pos, Field* field,  quint32 brainMs, QObject* parent = nullptr);
    void setView(FlyGraghicsItem* fv);
    qreal velocity() const;
    qreal distance() const;
    bool isDied()const;
    void stopFly();
public slots:
    void updatePosition(void);
private:
    QPoint posInField_;
    Field* field_;
    quint32 brainMs_;
    FlyGraghicsItem* fv_;
    quint32 liveTime_;
    qreal distance_;
    qreal velocity_;
    bool died;
    bool isFlyStopped;
    QTimer timer_;

    qint32 getRandVal(qint32 in);
    QPoint chooseNewCell();


};

#endif // FLY_H
