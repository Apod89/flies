#ifndef ANIMATEDPIXMAP_H
#define ANIMATEDPIXMAP_H
#include <QObject>
#include <QGraphicsPixmapItem>
class AnimatedPixmap : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
    Q_PROPERTY(QPixmap pixmap READ pixmap WRITE setPixmap)
public:
    explicit AnimatedPixmap(QGraphicsItem *graphicParent = nullptr, QObject *parent = nullptr) :
    QObject(parent),
    QGraphicsPixmapItem(graphicParent)
    {}
    explicit AnimatedPixmap(const QPixmap &pixmap, QGraphicsItem *graphicParent = nullptr, QObject *parent = nullptr):
    QObject(parent),
    QGraphicsPixmapItem(pixmap, graphicParent)
    {}
    virtual ~AnimatedPixmap() {}
};
#endif // ANIMATEDPIXMAP_H
