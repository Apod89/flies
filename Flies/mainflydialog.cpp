#include "mainflydialog.h"
#include <QVBoxLayout>
#include <QPixmap>
#include <QGraphicsView>
#include <QPushButton>
#include <QSlider>
#include <QColor>
#include <QMouseEvent>

#include <QDebug>
#include "field.h"
#include "settingslider.h"
#include "flygraghicsitem.h"
#include "flystatisticdialog.h"


MainFlyWidget::MainFlyWidget(QWidget *parent)
    : QWidget(parent)
{
    initUi();
    setMinimumSize(QSize(400, 600));
}


void MainFlyWidget::addNewFly(QPoint cell)
{
   PicPath picsPath= {"../Images/frame_0_delay-0.1s.png", "../Images/frame_1_delay-0.1s.png","../Images/frame_die.png"};
   if(field->bookCell(cell))
   {
    auto flyView = new FlyGraghicsItem(picsPath);
    fliesOnField.emplace_back(new Fly(cell,field.get(),flyIntelegence->value()), std::mem_fun(&QObject::deleteLater));
    fliesOnField.back()->setView(flyView);
    mainScene.addItem(flyView);
   }
}

void MainFlyWidget::drawGrid()
{
    for(int lineNo = 1;  lineNo < field->cellsCountInRaw() ; lineNo += 1)
    {
      int x = lineNo * field->cellWidth();
      int y = lineNo * field->cellHeight();
      staticGraphicItems.emplace_back(mainScene.addLine(x,0,x,field->height()));
      staticGraphicItems.emplace_back(mainScene.addLine( 0,y, field->width(),y));
    }
    view->setSceneRect(0,0,view->width(), view->height());
}

void MainFlyWidget::initUi()
{
    view = new QGraphicsView(&mainScene);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

    applyButton = new QPushButton("Apply", this);
    stopButton = new QPushButton("Stop", this);
    auto mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(view);
    fieldSizeInCells = new SettingSlider("Cell count", 2, 20, this);
    fieldSizeInCells->setMaximum(qMin(view->height(),view->width()) / FlyGraghicsItem::maxPicFlySize());
    cellFlyVolume = new SettingSlider("Cell volume (in flies)", 1, 20, this);
    flyIntelegence = new SettingSlider("Fly intelegence", 500, 5000, this);
    flyIntelegence->setTickInterval(500);
    mainLayout->addWidget(fieldSizeInCells);
    mainLayout->addWidget(cellFlyVolume);
    mainLayout->addWidget(flyIntelegence);
    auto buttonLayout = new QHBoxLayout(this);
    buttonLayout->addWidget(applyButton);
    buttonLayout->addWidget(stopButton);
    mainLayout->addLayout(buttonLayout);
    connect(applyButton, &QPushButton::clicked, this, &MainFlyWidget::onApplyButtonCLick);
    connect(stopButton, &QPushButton::clicked, this, &MainFlyWidget::onStopButtonCLick);
    setLayout(mainLayout);
}

void MainFlyWidget::clearField()
{
    if(field)
        field.reset(nullptr);
    staticGraphicItems.clear();
    mainScene.clear();
    fliesOnField.clear();
}

void MainFlyWidget::mousePressEvent(QMouseEvent *event)
{

    if(field && view->geometry().contains(event->pos()))
    {
        addNewFly(field->getCellWithPoint(event->pos()));
    }

}

void MainFlyWidget::resizeEvent(QResizeEvent *event)
{
   if(field)
       field->setFieldSize(view->size());
   fieldSizeInCells->setMaximum(qMin(view->height(),view->width()) / FlyGraghicsItem::maxPicFlySize());
   if(!(staticGraphicItems.size() == 0))
   {
       staticGraphicItems.clear();
       drawGrid();
   }
}

void MainFlyWidget::onApplyButtonCLick()
{
    clearField();
    field = std::make_unique<Field>(view->size(), fieldSizeInCells->value(),cellFlyVolume->value(),view);
    drawGrid();
}

void MainFlyWidget::onStopButtonCLick()
{
    FlyStatisticDialog*  dialog = new FlyStatisticDialog();
    for(auto& el : fliesOnField){
        el->stopFly();
        dialog->addItem(0,el->velocity(),el->distance(),el->isDied());
     }
    dialog->exec();
    dialog->deleteLater();
    clearField();
}

