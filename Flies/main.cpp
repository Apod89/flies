#include "mainflydialog.h"

#include <QApplication>
QVariant myPixmapInterpolator(const QPixmap &start, const QPixmap &end, qreal progress)
{
   (void)&end;
   (void)&progress;
   return start;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainFlyWidget w;
    w.show();
    qRegisterAnimationInterpolator<QPixmap>(myPixmapInterpolator);
    return a.exec();
}
