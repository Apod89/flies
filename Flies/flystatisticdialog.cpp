#include "flystatisticdialog.h"
#include <QVBoxLayout>
FlyStatisticDialog::FlyStatisticDialog(QWidget *parent, Qt::WindowFlags f) :
    QDialog(parent,f)
{
    this->setWindowTitle("Fly Statistic");
    setMinimumWidth(350);
    table = new QTableWidget(this);
    table->setColumnCount(3);
    table->setHorizontalHeaderLabels(QStringList({"Velocity","Distance","Died"}));
    okButton = new QPushButton("OK", this);
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(table);
    layout->addWidget(okButton);
    setLayout(layout);
    connect(okButton, &QPushButton::clicked,this, &QDialog::accept );

}

void FlyStatisticDialog::addItem(int no, qreal vel, qreal dist, bool die)
{
    table->insertRow(table->rowCount());
    addItemInternal(QString::number(vel,'f',2), 0);
    addItemInternal(QString::number(dist,'f',2), 1);
    addItemInternal(die?"x":"", 2);
 }

void FlyStatisticDialog::addItemInternal(const QString& cellText, int col)
{
    QTableWidgetItem* item = new QTableWidgetItem(cellText);
    item->setTextAlignment(Qt::AlignHCenter);
    table->setItem(table->rowCount()-1, col,item);
}
