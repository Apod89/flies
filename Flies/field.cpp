#include <QRandomGenerator>
#include <QPoint>
#include <QGraphicsView>
#include "field.h"


Field::Field(QSize size, int cellInRow, int cellCapacity, QGraphicsView* view):
    size_(size),
    cellInRowCount_(cellInRow),
    cellCapacity_(cellCapacity),
    view_(view),
    cellsCap_(cellInRow * cellInRow, cellCapacity)
{
    Q_ASSERT(cellInRowCount_ > 1);
    Q_ASSERT(cellCapacity_ > 0);
    cellSize_ = size / cellInRowCount_;
    Q_ASSERT(cellSize_.width() > 0 && cellSize_.height() > 0);

}

QPointF Field::cellPosOnScene(QPoint cell) const
{
    int xCell = cell.x();
    int yCell = cell.y();
    if(xCell >= cellInRowCount_)
        xCell = cellInRowCount_ - 1;
    if(yCell >= cellInRowCount_)
        yCell = cellInRowCount_ - 1;
    int xPos = xCell * cellSize_.width() + cellSize_.width() / 2;
    int yPos = yCell * cellSize_.height()+ cellSize_.height() / 2;
    return view_->mapToScene(QPoint(xPos, yPos));
}

QPoint Field::getCellWithPoint(QPoint point) const
{
     return QPoint(point.x() / cellSize_.width(), point.y() / cellSize_.height());
}

QPoint Field::getRandomFreeCell() const
{
    QPoint cell(QRandomGenerator::global()->bounded(cellInRowCount_),
                  QRandomGenerator::global()->bounded(cellInRowCount_));
    if(cellsCap_[convertPosToNo(cell)] != 0)
        return cell;
    for(int idx = 0; idx < cellsCap_.size(); ++idx)
    {
        if(cellsCap_[idx] != 0)
            return convertNoToPos(idx);
    }
    return QPoint(cellInRowCount_, cellInRowCount_);
}


bool Field::bookCell(QPoint cell)
{
    if(!checkCell(cell))
        return false;
    int cellNo = convertPosToNo(cell);
    if(cellsCap_[cellNo] == 0)
        return false;
    cellsCap_[cellNo] -= 1;
    return true;
}

int Field::cellsCountInRaw() const
{
    return cellInRowCount_;
}

int Field::width() const
{
    return size_.width();
}

int Field::height() const
{
    return size_.height();
}

int Field::cellWidth() const
{
    return cellSize_.width();
}

int Field::cellHeight() const
{
    return cellSize_.height();
}

void Field::setFieldSize(QSize size)
{
    size_ = size;
    cellSize_ = size / cellInRowCount_;
}

bool Field::freeAndBookCell(QPoint srcCell, QPoint dstCell)
{
    if(!(checkCell(srcCell) && checkCell(dstCell)))
        return false;
    int dstCellNo = convertPosToNo(dstCell);
    if(cellsCap_[dstCellNo] == 0)
        return false;
    int srcCellNo = convertPosToNo(srcCell);
    cellsCap_[srcCellNo] -= 1;
    cellsCap_[dstCellNo] += 1;
    return true;
}

int Field::convertPosToNo(QPoint cell) const
{
    Q_ASSERT(checkCell(cell) == true);
    return cell.x() * cellInRowCount_ + cell.y();
}

QPoint Field::convertNoToPos(int cellNo) const
{
    Q_ASSERT(cellNo >= 0 && cellNo < cellsCap_.size());
    QPoint res(cellNo % cellInRowCount_, cellNo / cellInRowCount_);
    return res;
}

bool Field::checkCell(QPoint cell) const
{
    if(cell.x() < 0 || cell.x() >= cellInRowCount_)
        return false;
    if(cell.y() < 0 || cell.y() >= cellInRowCount_)
        return false;
    return true;
}
