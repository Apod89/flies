#include <QLabel>
#include <QSlider>
#include <QGroupBox>
#include <QHBoxLayout>
#include "settingslider.h"

SettingSlider::SettingSlider(QString title, int min, int max, QWidget *parent) : QWidget(parent)
{
    groupBox = new QGroupBox(title, this);
    slider = new QSlider(Qt::Horizontal, this);
    slider->setRange(min, max);
    label = new QLabel(this);
    label->setNum(slider->value());
    auto layout = new QHBoxLayout();
    layout->addWidget(slider);
    layout->addWidget(label);
    groupBox->setLayout(layout);
    auto mainLayout = new QHBoxLayout();
    mainLayout->addWidget(groupBox);
    setLayout(mainLayout);
    connect(slider, &QSlider::valueChanged, label,QOverload<int>::of(&QLabel::setNum));
}

int SettingSlider::value() const
{
    return slider->value();
}

void SettingSlider::setTickInterval(int ti)
{
    slider->setTickInterval(ti);
}

void SettingSlider::setMaximum(int ti)
{
    slider->setMaximum(ti);
}
