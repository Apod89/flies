#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QVector2D>
#include <QtMath>
#include <QtDebug>
#include "flygraghicsitem.h"

FlyGraghicsItem::FlyGraghicsItem(PicPath picsPath, QGraphicsItem *parent):
    AnimatedPixmap(parent),
    framesPath(picsPath),
    picOrient(0, -1)
{
    initAnimation();
    setPixmap(QPixmap(framesPath.moveFrame1));
}


void FlyGraghicsItem::moveTo(qreal x, qreal y)
{
    rotateToCoord(x, y);
    moveAnim->setStartValue(pos());
    moveAnim->setEndValue(QPointF(x, y));
    comboAnim->start();
}

void FlyGraghicsItem::setDied()
{
    setPixmap(QPixmap(framesPath.diedFrame));
}

int FlyGraghicsItem::maxPicFlySize()
{
    return 70;
}

void FlyGraghicsItem::initAnimation()
{

    const qint32 animDurationMs = 600;
    wingAnim = new QPropertyAnimation(this, "pixmap");
    moveAnim = new QPropertyAnimation(this, "pos");
    wingAnim->setDuration(animDurationMs);
    moveAnim->setDuration(animDurationMs);
    for(int idx = 0; idx < 10; idx+=2)
    {
        wingAnim->setKeyValueAt(idx / 10.0, QPixmap(framesPath.moveFrame1));
        wingAnim->setKeyValueAt((idx + 1) / 10.0, QPixmap(framesPath.moveFrame2));
    }
    wingAnim->setKeyValueAt(0.99, QPixmap(framesPath.moveFrame1));
    comboAnim = new QParallelAnimationGroup(this);
    comboAnim->addAnimation(wingAnim);
    comboAnim->addAnimation(moveAnim);
}

void FlyGraghicsItem::rotateToCoord(qreal x, qreal y)
{
    auto boundingRect = this->boundingRect();
    QPointF med(boundingRect.width()/2, boundingRect.height()/2);
    QPointF dirVec = this->pos() + med;
    QVector2D dirVec2D(x - dirVec.x(), y - dirVec.y());

    qreal cosA = QVector2D::dotProduct(dirVec2D, picOrient)/ (dirVec2D.length() * picOrient.length());
    qreal sinA = picOrient.x() * dirVec2D.y() - picOrient.y()*dirVec2D.x();
    setTransformOriginPoint(med);
    qreal angel = qRadiansToDegrees(qAcos(cosA));
    if(sinA >= 0)
        angel += rotation();
    else
        angel = rotation() - angel;
    setRotation(angel);
    picOrient = dirVec2D;
}

